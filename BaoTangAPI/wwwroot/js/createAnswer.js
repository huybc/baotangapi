﻿R.Answer = {
    Init: function () {
        CKEDITOR.replace('Content');
        CKEDITOR.config.extraPlugins = "base64image";
        R.Answer.RegisterEvent();
        R.Answer.LoadIconUrl();
    },
    RegisterEvent: function () {
        $('#Media').off('change').on('change', function () {
            //alert(1);
            //debugger
            var fileInput = document.getElementById('Media');
            var file = fileInput.files[0];
            if (file != null) {
                R.Answer.UploadImage(file);

            }

        })
        $('#updateMedia').off('change').on('change', function () {
            //alert(1);
            //debugger
            var fileInput = document.getElementById('updateMedia');
            var file = fileInput.files[0];
            if (file != null) {
                R.Answer.UploadImage(file);
            }
        })
    },
    UploadImage: function (file) {
        var url = '/api/Extra/Upload';
        var dataFile = new FormData();
        dataFile.append('file', file);
        console.log(file);
        $.ajax({
            url: url,
            type: 'POST',
            data: dataFile,
            contentType: false,
            processData: false,
            success: function (res) {
                //alert(res);
                $('#Media').val(res);
                R.Answer.RegisterEvent();
            }
        })
    },
    LoadIconUrl: function () {
        var r = $('#Media').data('url');
        console.log(r);
        //if (r) {
        //    alert(1);
        //    $('#Icon').val(r);
        //}
    }
}
R.Answer.Init();

