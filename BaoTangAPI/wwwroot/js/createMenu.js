﻿R.CreateMenu = {
    Init: function () {
        CKEDITOR.replace('menu_des');
        CKEDITOR.config.extraPlugins = "base64image, iframe, div";
        //CKEDITOR.config.extraPlugins = "iframe";
        R.CreateMenu.RegisterEvent();
        R.CreateMenu.LoadIconUrl();
    },
    RegisterEvent: function () {
        $('#Icon').off('change').on('change', function () {
            alert(1);
            //debugger
            var fileInput = document.getElementById('Icon');
            var file = fileInput.files[0];
            if (file != null) {
                R.CreateMenu.UploadImage(file);
                
            }

        })
        $('#updateIcon').off('change').on('change', function () {
            //alert(1);
            //debugger
            var fileInput = document.getElementById('updateIcon');
            var file = fileInput.files[0];
            if (file != null) {
                R.CreateMenu.UploadImage(file);

            }

        })
        
    },
    UploadImage: function (file) {
        var url = '/api/Extra/Upload';
        var dataFile = new FormData();
        dataFile.append('file', file);
        console.log(file);
        $.ajax({
            url: url,
            type: 'POST',
            data: dataFile,
            contentType: false,
            processData: false,
            success: function (res) {
                //alert(res);
                $('#Icon').val(res);
                R.CreateMenu.RegisterEvent();
            }
        })
    },
    LoadIconUrl: function () {
        var r = $('#Icon').data('url');
        console.log(r);
        //if (r) {
        //    alert(1);
        //    $('#Icon').val(r);
        //}
    }
}
R.CreateMenu.Init();

