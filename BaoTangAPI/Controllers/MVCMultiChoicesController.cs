﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BaoTangAPI.Data;
using BaoTangAPI.Models;

namespace BaoTangAPI.Controllers
{
    public class MVCMultiChoicesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MVCMultiChoicesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: MVCMultiChoices
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.MultiChoices.Include(m => m.GetMenu).Include(m => m.GetMultiChoice);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: MVCMultiChoices/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multiChoice = await _context.MultiChoices
                .Include(m => m.GetMenu)
                .Include(m => m.GetMultiChoice)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (multiChoice == null)
            {
                return NotFound();
            }

            return View(multiChoice);
        }

        // GET: MVCMultiChoices/Create
        public IActionResult Create()
        {
            ViewData["MenuId"] = new SelectList(_context.Menus, "Id", "Title");
            ViewData["ParentId"] = new SelectList(_context.MultiChoices, "Id", "Topic");
            return View();
        }

        // POST: MVCMultiChoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Topic,Category,CreatedDate,StartDate,EndDate,Status,Order,QuestionType,ParentId,MenuId")] MultiChoice multiChoice)
        {
            if (ModelState.IsValid)
            {
                _context.Add(multiChoice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MenuId"] = new SelectList(_context.Menus, "Id", "Id", multiChoice.MenuId);
            ViewData["ParentId"] = new SelectList(_context.MultiChoices, "Id", "Id", multiChoice.ParentId);
            return View(multiChoice);
        }

        // GET: MVCMultiChoices/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multiChoice = await _context.MultiChoices.FindAsync(id);
            if (multiChoice == null)
            {
                return NotFound();
            }
            ViewData["MenuId"] = new SelectList(_context.Menus, "Id", "Title", multiChoice.MenuId);
            ViewData["ParentId"] = new SelectList(_context.MultiChoices, "Id", "Topic", multiChoice.ParentId);
            return View(multiChoice);
        }

        // POST: MVCMultiChoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Topic,Category,CreatedDate,StartDate,EndDate,Status,Order,QuestionType,ParentId,MenuId")] MultiChoice multiChoice)
        {
            if (id != multiChoice.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(multiChoice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MultiChoiceExists(multiChoice.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MenuId"] = new SelectList(_context.Menus, "Id", "Id", multiChoice.MenuId);
            ViewData["ParentId"] = new SelectList(_context.MultiChoices, "Id", "Id", multiChoice.ParentId);
            return View(multiChoice);
        }

        // GET: MVCMultiChoices/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multiChoice = await _context.MultiChoices
                .Include(m => m.GetMenu)
                .Include(m => m.GetMultiChoice)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (multiChoice == null)
            {
                return NotFound();
            }

            return View(multiChoice);
        }

        // POST: MVCMultiChoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var multiChoice = await _context.MultiChoices.FindAsync(id);
            _context.MultiChoices.Remove(multiChoice);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MultiChoiceExists(int id)
        {
            return _context.MultiChoices.Any(e => e.Id == id);
        }
    }
}
