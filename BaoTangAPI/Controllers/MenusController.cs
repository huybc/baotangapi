﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BaoTangAPI.Data;
using BaoTangAPI.Models;
using BaoTangAPI.VM;

namespace BaoTangAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenusController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MenusController(ApplicationDbContext context)
        {
            _context = context;
        }
        [HttpGet]
        [Route("MenuMinify/{parentId}")]
        public List<MenuVM> GetMenuMinifyParent(int parentId)
        {
            var result = new List<MenuVM>();
            if(parentId == 0)
            {
                //Lay parentId = 0
                var a = _context.Menus.Where(r => r.ParentId == null);
                foreach(var item in a)
                {
                    var r = new MenuVM();
                    r.Id = item.Id;
                    r.Title = item.Title;
                    r.Icon = item.Icon;
                    r.ParentId = item.ParentId;
                    result.Add(r);
                }
                return result;
            }
            if(parentId > 0)
            {
                var a = _context.Menus.Where(r => r.ParentId == parentId);
                foreach (var item in a)
                {
                    var r = new MenuVM();
                    r.Id = item.Id;
                    r.Title = item.Title;
                    r.Icon = item.Icon;
                    r.ParentId = item.ParentId;
                    result.Add(r);
                }
                return result;
            }
            return result;
        }
        

        // GET: api/Menus
        [HttpGet]
        public IEnumerable<Menu> GetMenus()
        {
            return _context.Menus;
        }

        // GET: api/Menus/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMenu([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var menu = await _context.Menus.FindAsync(id);

            if (menu == null)
            {
                return NotFound();
            }

            return Ok(menu);
        }

        // PUT: api/Menus/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMenu([FromRoute] int id, [FromBody] Menu menu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != menu.Id)
            {
                return BadRequest();
            }

            _context.Entry(menu).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MenuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Menus
        [HttpPost]
        public async Task<IActionResult> PostMenu([FromBody] Menu menu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Menus.Add(menu);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMenu", new { id = menu.Id }, menu);
        }

        // DELETE: api/Menus/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMenu([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var menu = await _context.Menus.FindAsync(id);
            if (menu == null)
            {
                return NotFound();
            }

            _context.Menus.Remove(menu);
            await _context.SaveChangesAsync();

            return Ok(menu);
        }

        private bool MenuExists(int id)
        {
            return _context.Menus.Any(e => e.Id == id);
        }
    }
}