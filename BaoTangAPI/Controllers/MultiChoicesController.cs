﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BaoTangAPI.Data;
using BaoTangAPI.Models;

namespace BaoTangAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MultiChoicesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MultiChoicesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/MultiChoices
        [HttpGet]
        public IEnumerable<MultiChoice> GetMultiChoices()
        {
            return _context.MultiChoices;
        }
        [HttpGet]
        [Route("getByMenuId/{id}")]
        public List<MultiChoice> GetMultiChoicesByMenuId(int id)
        {
            return _context.MultiChoices.Where(r => r.MenuId == id).ToList();
        }

        // GET: api/MultiChoices/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMultiChoice([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var multiChoice = await _context.MultiChoices.FindAsync(id);

            if (multiChoice == null)
            {
                return NotFound();
            }

            return Ok(multiChoice);
        }

        // PUT: api/MultiChoices/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMultiChoice([FromRoute] int id, [FromBody] MultiChoice multiChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != multiChoice.Id)
            {
                return BadRequest();
            }

            _context.Entry(multiChoice).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MultiChoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MultiChoices
        [HttpPost]
        public async Task<IActionResult> PostMultiChoice([FromBody] MultiChoice multiChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MultiChoices.Add(multiChoice);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMultiChoice", new { id = multiChoice.Id }, multiChoice);
        }

        // DELETE: api/MultiChoices/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMultiChoice([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var multiChoice = await _context.MultiChoices.FindAsync(id);
            if (multiChoice == null)
            {
                return NotFound();
            }

            _context.MultiChoices.Remove(multiChoice);
            await _context.SaveChangesAsync();

            return Ok(multiChoice);
        }

        private bool MultiChoiceExists(int id)
        {
            return _context.MultiChoices.Any(e => e.Id == id);
        }
    }
}