﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaoTangAPI.Data;
using BaoTangAPI.Enum;
using BaoTangAPI.Helper;
using BaoTangAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.Language.Extensions;
using Microsoft.EntityFrameworkCore;

namespace BaoTangAPI.Controllers
{
    public class GameDescriptionController : Controller
    {
        private ApplicationDbContext _context;
        public GameDescriptionController(ApplicationDbContext context) {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult StoryView(int menuId)
        {
            var r = _context.Menus.Find(menuId);
            if(r != null)
            {
                if (CheckMaxLeaf(menuId))
                {
                    if (r.LinkType == (int)MenuEnum.Story)
                    {
                        var body = "";
                        body = WebHelper.RenderStory(r.Description);
                        ViewBag.Body = body;
                        return View(r);
                    }
                }
                return BadRequest();
                
            }
            return BadRequest();
        }
        [HttpGet]
        public IActionResult PictureView(int menuId)
        {
            var r = _context.Menus.Find(menuId);
            if (r != null)
            {
                if (CheckMaxLeaf(menuId))
                {
                    if (r.LinkType == (int)MenuEnum.Photo)
                    {
                        var body = new List<string>();
                        body = WebHelper.RenderPicture(r.Description);
                        //ViewBag.Body = body;
                        return View(body);
                    }
                }
                return BadRequest();

            }
            return BadRequest();
        }
        [HttpGet]
        public IActionResult DoanChuView(int multichoiceId)
        {
            var mul = _context.MultiChoices.Find(multichoiceId);
            if(mul != null)
            {
                var res = new List<QuestionsAndAnswers>();
                var q = _context.Questions.Where(r => r.QuestionGroupId == mul.Id).Where(r => r.QType == (int)QuestionTypeEnum.DoanChu).ToList();
                foreach(var it in q)
                {
                    var a = new QuestionsAndAnswers();
                    a.QuestionTarget = it;
                    var x = _context.Answers.Where(r => r.QuestionId == it.Id).Include(r => r.GetQuestion).ToList();
                    a.ListAnswers = new List<Answer>();
                    a.ListAnswers.AddRange(x);
                    res.Add(a);
                }
                return View(res);
            }
            return BadRequest();
        }
        [HttpGet]
        public IActionResult DanhDanView(int multichoiceId)
        {
            var mul = _context.MultiChoices.Find(multichoiceId);
            if (mul != null)
            {
                var res = new List<QuestionsAndAnswers>();
                var q = _context.Questions.Where(r => r.QuestionGroupId == mul.Id).Where(r => r.QType == (int)QuestionTypeEnum.DanhDan).ToList();
                foreach (var it in q)
                {
                    var a = new QuestionsAndAnswers();
                    a.QuestionTarget = it;
                    var x = _context.Answers.Where(r => r.QuestionId == it.Id).Include(r => r.GetQuestion).ToList();
                    a.ListAnswers = new List<Answer>();
                    a.ListAnswers.AddRange(x);
                    res.Add(a);
                }
                return View(res);
            }
            return BadRequest();
        }
        [HttpGet]
        public IActionResult KeoThaView(int multichoiceId)
        {
            var mul = _context.MultiChoices.Find(multichoiceId);
            if (mul != null)
            {
                var res = new List<QuestionsAndAnswers>();
                var q = _context.Questions.Where(r => r.QuestionGroupId == mul.Id).Where(r => r.QType == (int)QuestionTypeEnum.KeoTha).ToList();
                foreach (var it in q)
                {
                    var a = new QuestionsAndAnswers();
                    a.QuestionTarget = it;
                    var x = _context.Answers.Where(r => r.QuestionId == it.Id).Include(r => r.GetQuestion).ToList();
                    a.ListAnswers = new List<Answer>();
                    a.ListAnswers.AddRange(x);
                    res.Add(a);
                }
                return View(res);
            }
            return BadRequest();
        }
        public IActionResult CauHoiView(int multichoiceId)
        {
            var mul = _context.MultiChoices.Find(multichoiceId);
            if (mul != null)
            {
                var res = new List<QuestionsAndAnswers>();
                var q = _context.Questions.Where(r => r.QuestionGroupId == mul.Id).Where(r => r.QType == (int)QuestionTypeEnum.CauHoi).ToList();
                foreach (var it in q)
                {
                    var a = new QuestionsAndAnswers();
                    a.QuestionTarget = it;
                    var x = _context.Answers.Where(r => r.QuestionId == it.Id).Include(r => r.GetQuestion).ToList();
                    a.ListAnswers = new List<Answer>();
                    a.ListAnswers.AddRange(x);
                    res.Add(a);
                }
                return View(res);
            }
            return BadRequest();
        }

        private bool CheckMaxLeaf(int menuId)
        {
            var x = _context.Menus.Find(menuId);
            if(x != null)
            {
                var checker = _context.Menus.Where(r => r.ParentId == menuId);
                if (checker.Count() > 0)
                    return false;
                else
                {
                    return true;
                }
            }
            return false;
        }
    }
}
