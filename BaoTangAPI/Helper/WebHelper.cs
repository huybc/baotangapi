﻿using BaoTangAPI.Enum;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BaoTangAPI.Helper
{
    public static class WebHelper
    {
        public static Dictionary<int, string> GetMenuType()
        {
            var r = new Dictionary<int, string>();
            foreach (MenuEnum foo in System.Enum.GetValues(typeof(MenuEnum)))
            {
                r.Add((int)foo, foo.ToString());
            }
            //var x = Enum.GetValues(typeof(SomeEnum)).Cast<SomeEnum>();

            return r;

        }
        public static Dictionary<int, string> GetMultiChoiceType()
        {
            var r = new Dictionary<int, string>();
            foreach (MultiChoiceEnum foo in System.Enum.GetValues(typeof(MultiChoiceEnum)))
            {
                r.Add((int)foo, foo.ToString());
            }
            //var x = Enum.GetValues(typeof(SomeEnum)).Cast<SomeEnum>();

            return r;

        }
        public static Dictionary<int, string> GetQuestionType()
        {
            var r = new Dictionary<int, string>();
            foreach (MultiChoiceEnum foo in System.Enum.GetValues(typeof(QuestionTypeEnum)))
            {
                r.Add((int)foo, foo.ToString());
            }
            //var x = Enum.GetValues(typeof(SomeEnum)).Cast<SomeEnum>();

            return r;

        }
        public static string RenderStory(string body)
        {
            HtmlDocument doc = new HtmlDocument();
            if (!string.IsNullOrEmpty(body))
            {
                doc.LoadHtml(body);
                var b = "";
                var page = doc.DocumentNode.SelectNodes("//div");
                if(page != null)
                {
                    foreach (var p in page)
                    {
                        p.AddClass("page-item");
                    }
                }
            }
            body = doc.DocumentNode.OuterHtml;
            return body;
        }
        public static List<string> RenderPicture(string body)
        {
            var result = new List<string>();

            if (!string.IsNullOrEmpty(body))
            {
                string pattern = @"src=""(.*?)""";
                string input = body;
                RegexOptions options = RegexOptions.Multiline;

                foreach (Match m in Regex.Matches(input, pattern, options))
                {
                    Console.WriteLine(m.Groups[1].Value);
                    result.Add(m.Groups[1].Value);
                }
            }
            return result;
        }
        public static string ClearHtmlTag(string origin)
        {
            HtmlDocument doc = new HtmlDocument();
            try
            {
                doc.LoadHtml(origin);
                return doc.DocumentNode.InnerText;
            }
            catch (Exception e)
            {
                return origin;
            }
        }
        public static string GetFirstImage(string body)
        {
            HtmlDocument doc = new HtmlDocument();
            if (!string.IsNullOrEmpty(body))
            {
                doc.LoadHtml(body);

                var firstImg = doc.DocumentNode.SelectSingleNode("//img");
                if (firstImg != null)
                {
                    var r = firstImg.GetAttributeValue("src", "").ToString();
                    //r = r.Replace("https://janhome.vn/wp-content/uploads", "");
                    return r;
                }
            }
            return "";
        }


    }
}
