﻿using BaoTangAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BaoTangAPI.Helper
{
    public class SelectListViewModel
    {
        public int E_Value { get; set; }
        public string E_Title { get; set; }
    }

    public class QuestionsAndAnswers
    {
        public Question QuestionTarget { get; set; }
        public List<Answer> ListAnswers { get; set; }
    }

    public class PhimDanViewModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
