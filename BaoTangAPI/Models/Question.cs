﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BaoTangAPI.Models
{
    public class Question
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Status { get; set; } = 1;
        public int QType { get; set; } = 1;
        public int Point { get; set; } = 0;
        public int QuestionGroupId { get; set; }

        [ForeignKey("QuestionGroupId")]
        public virtual MultiChoice GetMultiChoice { get; set; }
    }
}
