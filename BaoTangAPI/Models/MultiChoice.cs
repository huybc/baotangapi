﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BaoTangAPI.Models
{
    public class MultiChoice
    {
        [Key]
        public int Id { get; set; }
        public string Topic { get; set; }
        public string Category { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now.AddHours(1);
        public int Status { get; set; } = 1;
        public int Order { get; set; } = 1;
        public int QuestionType { get; set; } = 1;
        public int? ParentId { get; set; }
        public int MenuId { get; set; }

        [ForeignKey("ParentId")]
        public virtual MultiChoice GetMultiChoice { get; set; }

        [ForeignKey("MenuId")]
        public virtual Menu GetMenu { get; set; }
    }
}
