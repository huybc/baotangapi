﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BaoTangAPI.Models
{
    public class Copyright
    {
        [Key]
        public int CompanyName { get; set; }
        public int YearOfCopy { get; set; }
        public string LicenseType { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}
