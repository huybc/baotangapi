﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BaoTangAPI.Models
{
    
    public class Answer
    {
        [Key]
        public int Id { get; set; }
        public string Content { get; set; }
        public int isCorrect { get; set; } = 0;
        public int Status { get; set; } = 1;
        public int QuestionId { get; set; }
        public string Media { get; set; }

        [ForeignKey("QuestionId")]
        public virtual Question GetQuestion { get; set; }
    }
}
