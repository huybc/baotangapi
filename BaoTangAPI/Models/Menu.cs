﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BaoTangAPI.Models
{
    public class Menu
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public int Status { get; set; }
        public string Link { get; set; }
        public int LinkType { get; set; }
        public int? ParentId { get; set; }


        [ForeignKey("ParentId")]
        public virtual Menu GetMenu { get; set; }
    }
}
