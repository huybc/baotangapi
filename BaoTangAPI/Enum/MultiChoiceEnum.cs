﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BaoTangAPI.Enum
{
    public enum MultiChoiceEnum
    {
        DoanChu = 1,
        SapXep =2,
        DanhDan =3,
        KeoTha = 4,
        NoiHinh = 5,
        CauHoi = 6
    }
    public enum QuestionTypeEnum
    {
        DoanChu = 1,
        SapXep = 2,
        DanhDan = 3,
        KeoTha = 4,
        NoiHinh = 5,
        CauHoi = 6
    }
    public enum MenuEnum
    {
        Photo = 1,
        Video = 2,
        Story = 3, 
        MultiChoice = 4,
        Game = 5
    }
}
